package me.loper.mcskillclasses.listener;

import me.loper.mcskillclasses.McSkillClasses;
import me.loper.mcskillclasses.api.KitAPI;
import me.loper.mcskillclasses.utils.ChatUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PlayerListener implements Listener {
    private final McSkillClasses plugin;

    public PlayerListener(McSkillClasses plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onKick(PlayerKickEvent e) {
        Player p = e.getPlayer();
        if (KitAPI.getKitName(p) != null) {
            KitAPI.cleanup(p);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();

        if (KitAPI.getKitName(p) != null) {
            KitAPI.cleanup(p);
        }

        e.getDrops().removeIf(KitAPI::isKitItem);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {

            Player p = (Player) e.getEntity();
            if (p.getHealth() <= e.getDamage() &&
                    KitAPI.getKitName(p) != null) {
                KitAPI.cleanup(p);
            }
        }
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        Player p = e.getPlayer();
        if (KitAPI.getKitName(p) != null) {
            KitAPI.giveKitItems(p);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (KitAPI.getKitName(p) != null) {
            KitAPI.cleanup(p);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        KitAPI.cleanup(p);
    }

    @EventHandler
    public void onCraft(CraftItemEvent e) {
        Player p = (Player) e.getWhoClicked();

        for (ItemStack is : e.getInventory().getContents()) {
            if (KitAPI.isKitItem(is)) {
                String message = this.plugin.getConfig().getString("messages.error.crafting");
                p.sendMessage(ChatUtil.color(message));
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        Player p = e.getPlayer();
        if (KitAPI.isKitItem(e.getItemDrop().getItemStack())) {
            String message = this.plugin.getConfig().getString("messages.error.item_drop");
            p.sendMessage(ChatUtil.color(message));
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBypass(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        Inventory clicked = e.getInventory();

        if (clicked == p.getInventory()) {
            return;
        }

        ItemStack onCursor = e.getCursor();
        if (onCursor != null && KitAPI.isKitItem(onCursor)) {
            String message = this.plugin.getConfig().getString("messages.error.item_bypass");
            p.sendMessage(ChatUtil.color(message));
            e.setCancelled(true);
        }
    }
}


