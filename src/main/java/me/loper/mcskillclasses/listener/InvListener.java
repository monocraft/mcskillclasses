package me.loper.mcskillclasses.listener;

import me.loper.mcskillclasses.McSkillClasses;
import me.loper.mcskillclasses.api.KitAPI;
import me.loper.mcskillclasses.config.Kits;
import me.loper.mcskillclasses.inv.KitsInv;
import me.loper.mcskillclasses.sql.SQL;
import me.loper.mcskillclasses.utils.ChatUtil;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class InvListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onInvClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        Inventory inv = e.getInventory();
        ItemStack item = e.getCurrentItem();
        Kits kits = (McSkillClasses.getPlugin()).kits;

        if (inv.getTitle().contains(ChatUtil.color(kits.getCustomConfig().getString("Kits.inv-name")))) {
            e.setCancelled(true);
            if (item != null && item.hasItemMeta() && item.getItemMeta().hasDisplayName()) {
                if (kits.getCustomConfig().isConfigurationSection("Kits.list-of-kits")) {
                    FileConfiguration kitsConfig = McSkillClasses.getPlugin().kits.getCustomConfig();

                    for (String kitKey : kitsConfig.getConfigurationSection("Kits.list-of-kits").getKeys(false)) {
                        String kitName = kitsConfig.getString("Kits.list-of-kits." + kitKey + ".name");
                        if (item.getItemMeta().getDisplayName().contains(ChatUtil.color(kitName))) {
                            int bought = 0;

                            try {
                                McSkillClasses.sql.refreshConnect();

                                Statement st = SQL.connection.createStatement();
                                ResultSet res = st.executeQuery("SELECT * FROM `stats` WHERE `name`='" + p.getName().toLowerCase() + "'");
                                while (res.next()) {
                                    bought += res.getInt(kitKey);
                                }
                                st.close();
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                            FileConfiguration config = McSkillClasses.getPlugin().getConfig();
                            if (p.hasPermission("msc.kits." + kitKey) || bought > 0) {
                                if (KitAPI.getKitName(p) != null && KitAPI.getKitName(p).equals(kitKey)) {
                                    p.sendMessage(ChatUtil.color(config.getString("messages.kits.kit-already-chosen")));
                                    return;
                                }
                                if (!KitAPI.isFull(p)) {
                                    KitAPI.setKit(p, kitKey);
                                    String kitSelectedMessage = config.getString("messages.kits.kit-selected")
                                            .replaceAll("%kit%", ChatUtil.color(kitName));
                                    p.sendMessage(ChatUtil.color(kitSelectedMessage));
                                    KitAPI.cleanup(p);
                                    KitAPI.giveKitItems(p);
                                } else {
                                    p.sendMessage(ChatUtil.color(config.getString("messages.error.full-invenotry")));
                                }
                                p.closeInventory();
                                continue;
                            }

                            if (kitsConfig.getString("Kits.list-of-kits." + kitKey + ".permission") == null ||
                                kitsConfig.getString("Kits.list-of-kits." + kitKey + ".permission").equals("") ||
                                p.hasPermission(kitsConfig.getString("Kits.list-of-kits." + kitKey + ".permission"))
                            ) {
                                Economy economy = (McSkillClasses.getPlugin()).economy;

                                if ((int) economy.getBalance(p) >= kitsConfig.getInt("Kits.list-of-kits." + kitKey + ".price")) {
                                    KitAPI.setKit(p, kitKey);
                                    try {
                                        McSkillClasses.sql.refreshConnect();

                                        Statement st = SQL.connection.createStatement();
                                        ResultSet res = st.executeQuery("SELECT * FROM `stats` WHERE `name`='" + p.getName().toLowerCase() + "'");

                                        HashMap<String, Integer> classes = new HashMap<>();
                                        if (res.next()) {
                                            classes.put("Explorer", res.getInt("Explorer"));
                                            classes.put("Farmer", res.getInt("Farmer"));
                                            classes.put("Locator", res.getInt("Locator"));
                                            classes.put("Rogue", res.getInt("Rogue"));
                                            classes.put("Scout", res.getInt("Scout"));
                                        }
                                        classes.put(kitKey, 1);
                                        st.executeUpdate("REPLACE INTO `stats` (`name`, `explorer`, `farmer`, `locator`, `rogue`, `scout`) VALUES ('" + p.getName().toLowerCase() + "'," + classes.get("Explorer") + "," + classes.get("Farmer") + "," + classes.get("Locator") + "," + classes.get("Rogue") + "," + classes.get("Scout") + ");");
                                        st.close();
                                    } catch (SQLException ex) {
                                        ex.printStackTrace();
                                    }
                                    economy.withdrawPlayer(p, kitsConfig.getInt("Kits.list-of-kits." + kitKey + ".price"));
                                    p.sendMessage(ChatUtil.color(config.getString("messages.kits.you-have-bought-kit").replaceAll("%kit%", ChatUtil.color(kitsConfig.getString("Kits.list-of-kits." + kitKey + ".name")))));
                                    p.openInventory(KitsInv.KitsInventory(p));

                                    continue;
                                }
                                p.sendMessage(ChatUtil.color(config.getString("messages.eco.no-points")));

                                continue;
                            }

                            p.sendMessage(ChatUtil.color(config.getString("messages.kits.not-allowed-to-buy")));
                        }
                    }
                }

                if (item.getItemMeta().getDisplayName().equalsIgnoreCase(ChatUtil.color("&cУбрать класс"))) {
                    KitAPI.removeKit(p);
                    p.sendMessage(ChatUtil.color("&cТы убрал активный класс."));
                    p.closeInventory();
                }
            }
        }
    }
}


