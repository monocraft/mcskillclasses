package me.loper.mcskillclasses.api;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class KitActions
{
  public static void heal(Player p, double amount) {
    p.setHealth(Math.min((p.getHealth() + amount), p.getMaxHealth()));
  }

  
  public static void givePotionEffect(Player p, PotionEffectType type, int duration, int lvl) {
    p.addPotionEffect(new PotionEffect(type, duration * 20, lvl));
  }

  
  public static void givePotionEffect(Player p, PotionEffectType type, int lvl) {
    p.addPotionEffect(new PotionEffect(type, 2147483647, lvl));
  }
}


