package me.loper.mcskillclasses.api;

import me.loper.mcskillclasses.McSkillClasses;
import me.loper.mcskillclasses.utils.ChatUtil;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class KitAPI {

    public static HashMap<Player, String> kit = new HashMap<>();

    public static String getKitName(Player p) {
        return kit.getOrDefault(p, null);
    }


    public static void setKit(Player p, String s) {
        kit.put(p, s);
    }

    public static void removeKit(Player p) {
        cleanup(p);
        kit.remove(p);
    }


    public static ItemStack toItem(String paramString) {
        String[] itemData = paramString.split(" ");
        String materialName = itemData[0];

        itemData = Arrays.copyOfRange(itemData, 1, itemData.length);

        Material material = Material.getMaterial(materialName);
        ItemStack localItemStack = new ItemStack(material, 1);

        for (String dataString : itemData) {
            if (isNumber(dataString)) {
                localItemStack.setAmount(Integer.parseInt(dataString));
            }

            ItemMeta im = localItemStack.getItemMeta();
            String[] data = dataString.split(":");
            String key = data[0];

            String value = "";
            if (data.length > 1) {
                value = data[1];
            }

            switch (key) {
                case "enchant":
                    localItemStack.addUnsafeEnchantment(Enchantment.getByName(value), Integer.parseInt(data[2]));
                    break;
                case "name":
                    String displayName = ChatUtil.color(value.replaceAll("_", " "));
                    im.setDisplayName(displayName);
                    localItemStack.setItemMeta(im);
                    break;
                case "lore":
                    ArrayList<String> result = new ArrayList<>();

                    String lore = dataString.split(":")[1];
                    String[] loreLines = lore.split(";");
                    for (String line : loreLines) {
                        result.add(ChatUtil.color(line.replaceAll("_", " ")));
                    }

                    im.setLore(result);
                    localItemStack.setItemMeta(im);
                    break;
                case "durability":
                    String[] arrayOfString3 = dataString.split(":");
                    if (isNumber(arrayOfString3[1])) {
                        double d1 = localItemStack.getType().getMaxDurability() * (1.0D - Double.parseDouble(arrayOfString3[1]) / 100.0D);

                        localItemStack.setDurability((short) (int) d1);
                    }
                    break;
            }
        }

        return localItemStack;
    }


    public static boolean isNumber(String paramString) {
        try {
            Integer.parseInt(paramString);
            return true;
        } catch (Exception var2) {
            return false;
        }
    }

    public static void giveKitItems(Player p) {
        FileConfiguration config = McSkillClasses.getPlugin().kits.getCustomConfig();
        if (getKitName(p) != null && config.getStringList("Kits.list-of-kits." + getKitName(p) + ".items") != null) {

            for (String item : config.getStringList("Kits.list-of-kits." + getKitName(p) + ".items")) {

                if (getKitName(p).equals("Rogue")) {
                    ItemStack is = toItem(item);
                    ItemMeta itemMeta = is.getItemMeta();
                    itemMeta.setUnbreakable(true);
                    is.setItemMeta(itemMeta);
                    p.getInventory().addItem(is);

                    return;
                }

                p.getInventory().addItem(toItem(item));
            }
        }
    }


    public static List<ItemStack> getKitItems(String kit) {
        ArrayList<ItemStack> is = new ArrayList<>();
        FileConfiguration config = McSkillClasses.getPlugin().kits.getCustomConfig();

        if (config.getStringList("Kits.list-of-kits." + kit + ".items") != null) {
            for (String items : config.getStringList("Kits.list-of-kits." + kit + ".items")) {
                is.add(toItem(items));
            }
        }

        return is;
    }

    public static void cleanup(Player p) {
        p.getActivePotionEffects().forEach(effect -> p.removePotionEffect(effect.getType()));

        Arrays.stream(p.getInventory().getContents()).forEach(is -> {
            if (is != null && isKitItem(is)) {
                p.getInventory().removeItem(is);
            }
        });
    }

    public static boolean isKitItem(ItemStack is) {
        FileConfiguration config = (McSkillClasses.getPlugin()).kits.getCustomConfig();
        Set<String> listOfKits = config.getConfigurationSection("Kits.list-of-kits")
                .getKeys(false);

        for (String kit : listOfKits) {
            if (isKitItem(is, kit)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isKitItem(ItemStack is, String kit) {
        FileConfiguration config = (McSkillClasses.getPlugin()).kits.getCustomConfig();

        if (null == config.getStringList("Kits.list-of-kits." + kit + ".items")) {
            return false;
        }

        for (String items : config.getStringList("Kits.list-of-kits." + kit + ".items")) {
            ItemStack kitItemStack = toItem(items);

            if (!is.hasItemMeta() || !is.getItemMeta().hasDisplayName()) {
                continue;
            }

            if (!kitItemStack.hasItemMeta() || !kitItemStack.getItemMeta().hasDisplayName()) {
                continue;
            }

            String color = ChatUtil.color(kitItemStack.getItemMeta().getDisplayName());
            if (is.getItemMeta().getDisplayName().equalsIgnoreCase(color)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isFull(Player player) {
        return Arrays.stream(player.getInventory().getContents())
                .noneMatch(is -> null == is || is.getType().equals(Material.AIR));
    }

    public static boolean isEmpty(Inventory inv) {
        for (ItemStack item : inv.getContents()) {
            if (item != null) {
                return false;
            }
        }

        return true;
    }
}


