package me.loper.mcskillclasses.api;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import me.loper.mcskillclasses.McSkillClasses;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

public class WGApi {
    private WorldGuardPlugin worldGuardPlugin = null;

    public WGApi(McSkillClasses plugin) {
        Plugin worldGuardPlugin = plugin.getServer()
                .getPluginManager()
                .getPlugin("WorldGuard");

        if (null != worldGuardPlugin) {
            this.worldGuardPlugin = (WorldGuardPlugin) worldGuardPlugin;
        }
    }

    public boolean isExplorable(Location loc) {
        if (null == this.worldGuardPlugin) {
            return true;
        }

        RegionManager regionManager = worldGuardPlugin.getRegionManager(loc.getWorld());
        ApplicableRegionSet regions = regionManager.getApplicableRegions(loc);

        return regions.size() == 0 || regions.queryValue(null, DefaultFlag.PVP) == StateFlag.State.ALLOW;
    }
}
