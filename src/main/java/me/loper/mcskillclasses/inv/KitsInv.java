package me.loper.mcskillclasses.inv;

import me.loper.mcskillclasses.McSkillClasses;
import me.loper.mcskillclasses.api.KitAPI;
import me.loper.mcskillclasses.sql.SQL;
import me.loper.mcskillclasses.utils.ChatUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class KitsInv {
    public static Inventory KitsInventory(Player paramPlayer) {
        FileConfiguration config = (McSkillClasses.getPlugin()).kits.getCustomConfig();

        if (!config.isConfigurationSection("Kits.list-of-kits")) {
            return null;
        }
        Inventory localInventory = Bukkit.createInventory(null, 27, ChatUtil.color(config.getString("Kits.inv-name")));

        int i = 10;

        for (String kitName : config.getConfigurationSection("Kits.list-of-kits").getKeys(false)) {
            if (config.getBoolean("Kits.list-of-kits." + kitName + ".enable")) {
                ItemStack localItemStack = KitAPI.toItem(config.getString("Kits.list-of-kits." + kitName + ".item"));
                ItemMeta localItemMeta = localItemStack.getItemMeta();

                ArrayList<String> localArrayList = new ArrayList<>();
                for (String str2 : config.getStringList("Kits.list-of-kits." + kitName + ".lore")) {
                    localArrayList.add(ChatUtil.color(str2.replaceAll("%price%", config.getString("Kits.list-of-kits." + kitName + ".price"))));
                }

                int bought = 0;

                try {
                    McSkillClasses.sql.refreshConnect();

                    Statement st = SQL.connection.createStatement();
                    ResultSet res = st.executeQuery("SELECT * FROM `stats` WHERE `name`='" + paramPlayer.getName().toLowerCase() + "'");
                    while (res.next()) {
                        bought += res.getInt(kitName);
                    }
                    st.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                if (paramPlayer.hasPermission("msc.kits." + kitName) || bought > 0) {
                    localArrayList.add(ChatUtil.color("&a► Кликните для выбора."));
                } else {

                    localArrayList.add(ChatUtil.color("&3Цена: &a%price%$").replace("%price%", config.getString("Kits.list-of-kits." + kitName + ".price")));
                    localArrayList.add(ChatUtil.color("&c► Кликните для покупки."));
                }
                localItemMeta.setDisplayName(ChatUtil.color(config.getString("Kits.list-of-kits." + kitName + ".name")));
                localItemMeta.setLore(localArrayList);
                localItemStack.setItemMeta(localItemMeta);
                localInventory.setItem(i, localItemStack);
                i++;
            }
        }
        if (KitAPI.getKitName(paramPlayer) != null) {

            i++;
            ItemStack back = new ItemStack(Material.BARRIER, 1);
            ItemMeta im = back.getItemMeta();
            im.setDisplayName(ChatUtil.color("&cУбрать класс"));

            ArrayList<String> lore = new ArrayList<>();
            lore.add(ChatUtil.color(" "));
            lore.add(ChatUtil.color("&cКликните для сброса"));
            lore.add(ChatUtil.color("&aактивного класса."));
            im.setLore(lore);
            back.setItemMeta(im);
            localInventory.setItem(i, back);
        }
        return localInventory;
    }


    private static Integer getMultipleNumber(Integer paramInteger) {
        if (paramInteger.intValue() > 36) {
            return Integer.valueOf(45);
        }
        if (paramInteger.intValue() > 27) {
            return Integer.valueOf(36);
        }
        if (paramInteger.intValue() > 18) {
            return Integer.valueOf(27);
        }
        if (paramInteger.intValue() > 9) {
            return Integer.valueOf(18);
        }
        return Integer.valueOf(9);
    }
}


