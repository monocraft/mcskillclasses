package me.loper.mcskillclasses;

import me.loper.mcskillclasses.ability.*;
import me.loper.mcskillclasses.api.KitAPI;
import me.loper.mcskillclasses.api.WGApi;
import me.loper.mcskillclasses.command.ClassCommand;
import me.loper.mcskillclasses.config.Kits;
import me.loper.mcskillclasses.listener.InvListener;
import me.loper.mcskillclasses.listener.PlayerListener;
import me.loper.mcskillclasses.sql.SQL;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.logging.Logger;

public class McSkillClasses extends JavaPlugin {
    private static McSkillClasses plugin = null;
    public Logger logger = Logger.getLogger("Minecraft");
    public Kits kits = new Kits();
    public Economy economy = null;

    private final WGApi wgApi = new WGApi(this);
    public static SQL sql;

    public void onEnable() {
        plugin = this;
        setupEconomy();
        loadConfiguration();

        Bukkit.getPluginManager().registerEvents(new InvListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);

        Bukkit.getPluginManager().registerEvents(new Scout(), this);
        Bukkit.getPluginManager().registerEvents(new Explorer(this), this);
        Bukkit.getPluginManager().registerEvents(new Farmer(), this);
        Bukkit.getPluginManager().registerEvents(new Locator(this), this);
        Bukkit.getPluginManager().registerEvents(new Rogue(this), this);

        getCommand("class").setExecutor(new ClassCommand());

        sql = new SQL(this);
    }


    public void onDisable() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (KitAPI.getKitName(p) != null) {
                KitAPI.cleanup(p);
            }
        }
    }


    public static McSkillClasses getPlugin() {
        return plugin;
    }


    private void setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
        if (economyProvider != null) {
            this.economy = economyProvider.getProvider();
        }
    }


    public void loadConfiguration() {
        File configFile = new File(getDataFolder(), "config.yml");

        if (!configFile.exists()) {
            getConfig().set("messages.info.reload", "&aКонфигурация успешно перезагружена!");
            getConfig().set("messages.error.no-cmd", "&eПохоже, Вы где-то ошиблись. Введите /class для помощи.");
            getConfig().set("messages.error.no-permissions", "&cУ Вас нет прав для выполнения данного действия.");

            getConfig().set("messages.eco.no-points", "&cУ Вас не хватает &aэмов &cдля совершения данной покупки.");

            getConfig().set("messages.kits.kit-already-chosen", "&cВы уже выбрали этот класс!");
            getConfig().set("messages.kits.kit-selected", "&aВы выбрали класс %kit%");
            getConfig().set("messages.kits.you-have-bought-kit", "&aВы успешно купили класс %kit%!");
            getConfig().set("messages.kits.not-allowed-to-buy", "&cДанный класс недоступен для Вас.");
            getConfig().set("messages.rogue.no-permission-to-steal", "&cТы не можешь воровать предметы у игрока &e%caught%");
            getConfig().set("messages.rogue.region-pvp-deny", "&cТы не можешь воровать предметы у игрока &e%caught%");
            getConfig().set("messages.explorer.region-pvp-deny", "&cТы сможешь использовать эту способность через &b%time% сек.");
        }

        getConfig().options().copyDefaults(true);
        saveConfig();

        this.kits.customYml = new File(getDataFolder(), "kits.yml");
        this.kits.customConfig = YamlConfiguration.loadConfiguration(this.kits.customYml);

        this.kits.copyDefaults();
        this.kits.saveCustomConfig();

        this.logger.info("[" + plugin.getName() + "] " + "Configuration successfully loaded!");
    }

    public WGApi getWgAPI() {
        return this.wgApi;
    }
}


