package me.loper.mcskillclasses.sql;

import me.loper.mcskillclasses.McSkillClasses;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SQL {
    McSkillClasses main;
    public static Connection connection;

    public SQL(McSkillClasses plugin) {
        this.main = McSkillClasses.getPlugin();
        testConnection();
    }

    private void testConnection() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection(
                    "JDBC:sqlite:" + this.main.getDataFolder().getAbsolutePath() + File.separator + "stats.db");
            Statement statement = connection.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS `stats` (`name` VARCHAR(255) NOT NULL PRIMARY KEY,`explorer` INT, `farmer` INT, `locator` INT, `rogue` INT, `scout` INT)");
            statement.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void refreshConnect() throws SQLException {
        if (connection == null) {
            connection = DriverManager.getConnection("JDBC:sqlite:" + this.main.getDataFolder().getAbsolutePath() + File.separator + "stats.db");
        }
    }
}


