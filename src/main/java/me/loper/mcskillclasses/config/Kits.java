package me.loper.mcskillclasses.config;

import me.loper.mcskillclasses.McSkillClasses;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.util.logging.Logger;

public class Kits {
    public File customYml;
    public FileConfiguration customConfig;

    public void saveCustomConfig() {
        Logger logger = (McSkillClasses.getPlugin()).logger;

        try {
            this.customConfig.save(this.customYml);
        } catch (IOException localIOException) {
            logger.warning("[MSC] Cannot load KITS.yml FILE!");
            logger.warning("[MSC] ERROR: " + localIOException);
        }
    }

    public FileConfiguration getCustomConfig() {
        return this.customConfig;
    }

    public File getCustomFile() {
        return this.customYml;
    }

    public void copyDefaults() {
        Reader defConfigStream = new InputStreamReader(McSkillClasses.getPlugin().getResource("kits.yml"));
        YamlConfiguration localYamlConfiguration = YamlConfiguration.loadConfiguration(defConfigStream);

        this.customConfig.setDefaults(localYamlConfiguration);

        this.customConfig.options().copyDefaults(true);
        this.customConfig.options().copyDefaults();
    }


    public void reloadCustomConfig() {
        if (this.customYml == null) {
            this.customYml = new File(McSkillClasses.getPlugin().getDataFolder(), "kits.yml");
        }

        this.customConfig = YamlConfiguration.loadConfiguration(this.customYml);

        InputStream resource = McSkillClasses.getPlugin().getResource("kits.yml");
        Reader localInputStream = new InputStreamReader(resource);

        YamlConfiguration localYamlConfiguration = YamlConfiguration
                .loadConfiguration(localInputStream);
        this.customConfig.setDefaults(localYamlConfiguration);
    }
}


