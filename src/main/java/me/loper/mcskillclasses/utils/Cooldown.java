package me.loper.mcskillclasses.utils;

import java.util.HashMap;
import java.util.Map;

public class Cooldown {
    private static final Map<String, Cooldown> cooldowns = new HashMap<>();

    private long start;
    private final int timeInSeconds;
    private final String pl;
    private final String cooldownName;

    public Cooldown(String pl, String cooldownName, int timeInSeconds) {
        this.pl = pl;
        this.cooldownName = cooldownName;
        this.timeInSeconds = timeInSeconds;
    }

    public static boolean isInCooldown(String pl, String cooldownName) {
        if (getTimeLeft(pl, cooldownName) >= 1) {
            return true;
        }
        stop(pl, cooldownName);
        return false;
    }


    private static void stop(String pl, String cooldownName) {
        cooldowns.remove(pl + cooldownName);
    }


    private static Cooldown getCooldown(String pl, String cooldownName) {
        return cooldowns.get(pl + cooldownName);
    }

    public static int getTimeLeft(String pl, String cooldownName) {
        Cooldown cooldown = getCooldown(pl, cooldownName);
        int f = -1;
        if (cooldown != null) {

            long now = System.currentTimeMillis();
            long cooldownTime = cooldown.start;
            int r = (int) (now - cooldownTime) / 1000;
            f = (r - cooldown.timeInSeconds) * -1;
        }
        return f;
    }


    public void start() {
        this.start = System.currentTimeMillis();
        cooldowns.put(this.pl + this.cooldownName, this);
    }
}


