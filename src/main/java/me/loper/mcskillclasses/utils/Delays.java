package me.loper.mcskillclasses.utils;

import me.loper.mcskillclasses.McSkillClasses;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.*;

public class Delays {
    private static Delays instance;

    public static Delays getInstance() {
        if (instance == null) {
            instance = new Delays();
        }
        return instance;
    }

    private int counter = 0;

    private final Map<String, Integer> delayIDs;

    private final Map<Integer, DelayUpdate> delayHandlers;
    private final List<Delay> delays;

    private Delays() {
        this.delays = new ArrayList<>();
        this.delayIDs = new HashMap<>();
        this.delayHandlers = new HashMap<>();
        Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin) McSkillClasses.getPlugin(), new DelayUpdater(this), 20L, 20L);
    }


    private class DelayUpdater
            implements Runnable {
        private final Delays d;

        public DelayUpdater(Delays d) {
            this.d = d;
        }


        public void run() {
            Iterator<Delays.Delay> it = this.d.delays.iterator();
            while (it.hasNext()) {

                Delays.Delay d = it.next();
                if (d.update() <= 0) {
                    it.remove();
                }
            }
        }
    }


    public void createNewDelay(String delayName, DelayUpdate handler) {
        if (!this.delayIDs.containsKey(delayName.toLowerCase())) {

            this.delayIDs.put(delayName.toLowerCase(), Integer.valueOf(this.counter));
            this.delayHandlers.put(Integer.valueOf(this.counter), handler);
            this.counter++;
        }
    }


    public boolean hasActiveDelay(Player p, String delayType) {
        for (Delay d : this.delays) {
            if (d.playerID.equals(p.getUniqueId()) && d.DelayID == ((Integer) this.delayIDs.get(delayType.toLowerCase())).intValue() && System.currentTimeMillis() < d.endTime) {
                return true;
            }
        }
        return false;
    }


    public void addDelay(Player p, long endTime, String delay) {
        Integer id = this.delayIDs.get(delay.toLowerCase());
        Delay d = new Delay(p.getUniqueId(), id.intValue(), endTime, this.delayHandlers.get(id));
        this.delays.add(d);
    }


    private class Delay {
        private UUID playerID;
        private int DelayID;
        private long endTime;
        private DelayUpdate handler;

        public Delay(UUID playerID, int ID, long end, DelayUpdate handler) {
            this.playerID = playerID;
            this.DelayID = ID;
            this.endTime = end;
            this.handler = handler;
        }


        public int update() {
            Player p = Bukkit.getPlayer(this.playerID);
            if (p != null) {

                int seconds = (int) ((this.endTime - System.currentTimeMillis()) / 1000L);
                this.handler.update(p, seconds);
                return seconds;
            }
            return 0;
        }


        public int hashCode() {
            int prime = 31;
            int result = 1;
            result = 31 * result + this.DelayID;
            result = 31 * result + ((this.playerID == null) ? 0 : this.playerID.hashCode());
            return result;
        }


        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            Delay other = (Delay) obj;
            if (this.DelayID != other.DelayID) {
                return false;
            }
            if (this.playerID == null) {

                if (other.playerID != null) {
                    return false;
                }
            } else if (!this.playerID.equals(other.playerID)) {
                return false;
            }
            return true;
        }
    }
}


