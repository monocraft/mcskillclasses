package me.loper.mcskillclasses.utils;

import org.bukkit.ChatColor;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.ArrayList;
import java.util.List;

public class ChatUtil {
    public static String color(@NonNull String paramString) {
        return ChatColor.translateAlternateColorCodes('&', paramString);
    }

    public static List<String> color(List<String> paramList) {
        List<String> colored = new ArrayList<>();
        if (paramList != null) {
            for (String str : paramList) {
                colored.add(color(str));
            }
        } else {
            colored.add("null");
        }
        return colored;
    }
}


