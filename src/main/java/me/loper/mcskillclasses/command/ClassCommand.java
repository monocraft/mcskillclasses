package me.loper.mcskillclasses.command;

import me.loper.mcskillclasses.McSkillClasses;
import me.loper.mcskillclasses.inv.KitsInv;
import me.loper.mcskillclasses.utils.ChatUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;


public class ClassCommand implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (!cmd.getName().equalsIgnoreCase("class")) {
            return false;
        }

        if (args.length == 0 && sender instanceof Player) {
            Player p = (Player) sender;
            p.openInventory(KitsInv.KitsInventory(p));
            return true;
        }

        FileConfiguration config = McSkillClasses.getPlugin().getConfig();
        if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
            if (sender.isOp() || sender.hasPermission("class.reload")) {
                McSkillClasses.getPlugin().reloadConfig();
                sender.sendMessage(ChatUtil.color(config.getString("messages.info.reload")));
            } else {
                sender.sendMessage(ChatUtil.color(config.getString("messages.error.no-permissions")));
            }
            return true;
        }

        sender.sendMessage(ChatUtil.color(config.getString("messages.error.no-cmd")));

        return true;
    }
}


