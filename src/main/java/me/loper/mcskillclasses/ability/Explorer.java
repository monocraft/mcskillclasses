package me.loper.mcskillclasses.ability;

import me.loper.mcskillclasses.McSkillClasses;
import me.loper.mcskillclasses.api.KitAPI;
import me.loper.mcskillclasses.api.WGApi;
import me.loper.mcskillclasses.utils.ChatUtil;
import me.loper.mcskillclasses.utils.Cooldown;
import me.loper.mcskillclasses.utils.InvSaver;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Explorer implements Listener {

    public static List<String> openedList = new ArrayList<>();
    private final McSkillClasses plugin;

    public Explorer(McSkillClasses plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onInteract(PlayerInteractEvent e) throws IOException {
        Player p = e.getPlayer();
        ItemStack itemInHand = p.getInventory().getItemInHand();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType() == Material.CHEST) {
            Chest chest = (Chest) e.getClickedBlock().getState();
            String kit = KitAPI.getKitName(p);

            if (kit != null && kit.equals("Explorer")) {
                for (ItemStack is : KitAPI.getKitItems(kit)) {
                    if (itemInHand == null || !itemInHand.hasItemMeta() || !itemInHand.getItemMeta().hasDisplayName()) {
                        return;
                    }
                    FileConfiguration config = this.plugin.getConfig();

                    if (itemInHand.getItemMeta().getDisplayName().equals(is.getItemMeta().getDisplayName())) {
                        e.setCancelled(true);
                        if (Cooldown.isInCooldown(p.getName(), "Explorer")) {
                            p.sendMessage(ChatUtil.color(config.getString("messages.skill.cooldown-usage").replace("%time%", String.valueOf(Cooldown.getTimeLeft(p.getName(), "Explorer")))));
                            return;
                        }

                        if (!this.plugin.getWgAPI().isExplorable(chest.getLocation())) {
                            p.sendMessage(ChatUtil.color(config.getString("messages.explorer.region-pvp-deny")));
                            return;
                        }

                        openedList.add(p.getName());
                        String savedinv = InvSaver.saveInv(chest.getInventory());
                        Inventory savedchest = InvSaver.loadInv(savedinv);
                        InvSaver.openSavedInv(p, savedchest, ChatUtil.color("Просмотр сундука"));

                        Cooldown c = new Cooldown(p.getName(), "Explorer", 120);
                        c.start();
                    }
                }
            }
        }
    }


    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (openedList.contains(p.getName())) {

            e.setResult(Event.Result.DENY);
            e.setCancelled(true);
        }
    }


    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        Player p = (Player) e.getPlayer();
        openedList.remove(p.getName());
    }


    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        openedList.remove(p.getName());
    }


    @EventHandler
    public void onKick(PlayerKickEvent e) {
        Player p = e.getPlayer();
        openedList.remove(p.getName());
    }

}
