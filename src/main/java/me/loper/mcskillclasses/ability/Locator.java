package me.loper.mcskillclasses.ability;

import me.loper.mcskillclasses.McSkillClasses;
import me.loper.mcskillclasses.api.KitAPI;
import me.loper.mcskillclasses.utils.ChatUtil;
import me.loper.mcskillclasses.utils.Cooldown;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;


public class Locator implements Listener {
    private final McSkillClasses plugin;

    public Locator(McSkillClasses plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if ((e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) &&
                KitAPI.getKitName(p) != null &&
                KitAPI.getKitName(p).equals("Locator")) {
            for (ItemStack is : KitAPI.getKitItems(KitAPI.getKitName(p))) {

                if (p.getItemInHand() == null || !p.getItemInHand().hasItemMeta() || !p.getItemInHand().getItemMeta().hasDisplayName()) {
                    return;
                }
                if (p.getItemInHand().getItemMeta().getDisplayName().equals(is.getItemMeta().getDisplayName())) {
                    e.setCancelled(true);

                    if (Cooldown.isInCooldown(p.getName(), "Locator")) {
                        p.sendMessage(ChatUtil.color(this.plugin.getConfig().getString("messages.skill.cooldown-usage").replace("%time%", String.valueOf(Cooldown.getTimeLeft(p.getName(), "Locator")))));
                        return;
                    }

                    if (getNearestPlayer(p) != null) {
                        p.sendMessage(ChatUtil.color("&cИгрок: &e" + Objects.requireNonNull(getNearestPlayer(p)).getName() + "&c. Координаты XYZ: &e" +
                                Objects.requireNonNull(getNearestPlayer(p)).getLocation().getBlockX() + "&c, &e" +
                                Objects.requireNonNull(getNearestPlayer(p)).getLocation().getBlockY() + "&c, &e" +
                                Objects.requireNonNull(getNearestPlayer(p)).getLocation().getBlockZ() + "&c."));
                    } else {
                        p.sendMessage(ChatUtil.color("&cВ этом мире нет других игроков."));
                    }

                    new Cooldown(p.getName(), "Locator", 45).start();
                }
            }
        }
    }


    public static Player getNearestPlayer(Player checkNear) {
        Player nearest = null;
        if (checkNear.getWorld().getPlayers().size() > 1) {
            for (Player p : checkNear.getWorld().getPlayers()) {
                if (!p.getName().equals(checkNear.getName())) {
                    if (nearest == null) {
                        nearest = p;
                        continue;
                    }
                    if (p.getLocation().distance(checkNear.getLocation()) < nearest.getLocation().distance(checkNear.getLocation())) {
                        nearest = p;
                    }
                }
            }
        } else {
            return null;
        }
        return nearest;
    }
}


