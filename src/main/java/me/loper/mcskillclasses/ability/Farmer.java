package me.loper.mcskillclasses.ability;

import java.util.Random;

import me.loper.mcskillclasses.api.KitAPI;
import org.bukkit.CropState;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;


public class Farmer implements Listener {
  @EventHandler
  public void onBreak(BlockBreakEvent e) {
    Player p = e.getPlayer();
    Block b = e.getBlock();

    Random rand = new Random();
    if (KitAPI.getKitName(p) != null &&
      KitAPI.getKitName(p).equals("Farmer") && (
      b.getType() == Material.CROPS || b.getType() == Material.CARROT || b.getType() == Material.POTATO) &&
      b.getData() == CropState.RIPE.getData()) {
      
      b.breakNaturally();
      int cropsDrops = rand.nextInt(50) + 1;
      if (cropsDrops == 1) {
        
        Random ghastRand = new Random();
        int ghastTearDrop = ghastRand.nextInt(7) + 1;
        if (ghastTearDrop == 2) {
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.GHAST_TEAR, 1));
        }
      } 
      if (cropsDrops == 22) {
        
        Random gdRand = new Random();
        int gd = gdRand.nextInt(6) + 1;
        if (gd == 1) {
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.GOLD_NUGGET, rand.nextInt(4) + 1));
        }
        if (gd == 2) {
          
          ItemStack BONE_MEAL = new ItemStack(Material.INK_SACK);
          BONE_MEAL.setDurability((short)15);
          BONE_MEAL.setAmount(rand.nextInt(6) + 2);
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), BONE_MEAL);
        } 
      } 
      if (cropsDrops == 30) {
        
        Random oreRand = new Random();
        int oreDrop = oreRand.nextInt(7) + 1;
        if (oreDrop == 1) {
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.GOLD_ORE, rand.nextInt(1) + 1));
        }
        if (oreDrop == 2) {
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.IRON_ORE, rand.nextInt(3) + 1));
        }
        if (oreDrop == 3) {
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.DIAMOND, 1));
        }
      } 
      if (cropsDrops == 28) {
        
        Random potionRand = new Random();
        int potionIng = potionRand.nextInt(3) + 1;
        if (potionIng == 1) {
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.NETHER_STALK, 1));
        }
        if (potionIng == 2) {
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.SULPHUR, 1));
        }
      } 
      if (cropsDrops == 13) {
        
        Random accessoryRand = new Random();
        int accessory = accessoryRand.nextInt(3) + 1;
        if (accessory == 1) {
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.BOOK, 1));
        }
        if (accessory == 2) {
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.EXP_BOTTLE, rand.nextInt(3) + 1));
        }
        if (accessory == 3) {
          
          int appleRand = rand.nextInt(2) + 1;
          if (appleRand == 1) {
            b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.APPLE, 1));
          }
        } 
      } 
      if (cropsDrops == 17) {
        
        Random hoeRand = new Random();
        int hoe = hoeRand.nextInt(5) + 1;
        if (hoe == 1) {
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.STONE_SWORD, 1));
        }
        if (hoe == 2)
          b.getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.STONE_HOE, 1)); 
      } 
    } 
  }
}


