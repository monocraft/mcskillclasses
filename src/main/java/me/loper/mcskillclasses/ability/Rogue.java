package me.loper.mcskillclasses.ability;

import me.loper.mcskillclasses.McSkillClasses;
import me.loper.mcskillclasses.api.KitAPI;
import me.loper.mcskillclasses.api.WGApi;
import me.loper.mcskillclasses.utils.ChatUtil;
import me.loper.mcskillclasses.utils.Cooldown;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Rogue implements Listener {

    private final McSkillClasses plugin;

    public Rogue(McSkillClasses plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerHitFishingRodScorpion(PlayerFishEvent e) {
        if (!(e.getCaught() instanceof Player)) {
            return;
        }

        FileConfiguration config = this.plugin.getConfig();
        Player caught = (Player) e.getCaught();
        Player p = e.getPlayer();

        if (caught.hasPermission("mcs.rogue.exempt")) {
            String noPermissionToSteal = config.getString("messages.rogue.no-permission-to-steal")
                    .replace("%caught%", caught.getName());
            p.sendMessage(ChatUtil.color(noPermissionToSteal));
            return;
        }

        WGApi wgAPI = this.plugin.getWgAPI();

        if (!wgAPI.isExplorable(p.getLocation()) || !wgAPI.isExplorable(caught.getLocation())) {
            p.sendMessage(ChatUtil.color(config.getString("messages.rogue.region-pvp-deny")));
            return;
        }

        if (KitAPI.getKitName(p) == null || !KitAPI.getKitName(p).equals("Rogue")) {
            return;
        }

        for (ItemStack is : KitAPI.getKitItems(KitAPI.getKitName(p))) {
            ItemStack itemInHand = p.getItemInHand();

            if (itemInHand == null || !itemInHand.hasItemMeta() || !itemInHand.getItemMeta().hasDisplayName()) {
                continue;
            }

            String displayName = itemInHand.getItemMeta().getDisplayName();

            if (!displayName.equals(is.getItemMeta().getDisplayName())) {
                continue;
            }

            if (KitAPI.isEmpty(caught.getInventory())) {
                p.sendMessage(ChatUtil.color("&cИнвентарь жертвы пуст."));
                continue;
            }

            if (Cooldown.isInCooldown(p.getName(), "Rogue")) {
                String timeLeft = String.valueOf(Cooldown.getTimeLeft(p.getName(), "Rogue"));
                String message = this.plugin.getConfig().getString("messages.skill.cooldown-usage")
                        .replace("%time%", timeLeft);
                p.sendMessage(ChatUtil.color(message));
                return;
            }

            this.stealItem(caught, p);
        }
    }

    private void stealItem(Player caught, Player p) {
        List<ItemStack> itemsToSteal = new ArrayList<>();
        for (ItemStack is : caught.getInventory().getContents()) {
            if (null != is && !is.getType().equals(Material.AIR)) {
                itemsToSteal.add(is);
            }
        }

        Random random = new Random();
        float chance = random.nextFloat();
        if (chance > 0.2F) {
            return;
        }

        int randIt = random.nextInt(itemsToSteal.size());
        ItemStack reward = itemsToSteal.get(randIt);

        new Cooldown(p.getName(), "Rogue", 45).start();

        if (caught.isOnline() && reward != null) {

            if (!KitAPI.isFull(p)) {
                p.sendMessage(ChatUtil.color("&aТы украл &e" + reward.getType().toString() + " &aу игрока &e" + caught.getName()));
                caught.getInventory().removeItem(reward);
                p.getInventory().addItem(reward);
                return;
            }

            String fullInventoryMessage = this.plugin.getConfig()
                    .getString("messages.rogue.full-inventory")
                    .replaceAll("%item_type%", reward.getType().toString());
            p.sendMessage(ChatUtil.color(fullInventoryMessage));
            caught.getInventory().removeItem(reward);
            p.getWorld().dropItem(caught.getLocation(), reward);
        }
    }
}


