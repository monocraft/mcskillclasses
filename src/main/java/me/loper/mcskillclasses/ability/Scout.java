package me.loper.mcskillclasses.ability;

import me.loper.mcskillclasses.api.KitAPI;
import me.loper.mcskillclasses.api.KitActions;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffectType;


public class Scout
  implements Listener
{
  @EventHandler
  public void onMove(PlayerMoveEvent e) {
    Player p = e.getPlayer();
    if (KitAPI.getKitName(p) != null && KitAPI.getKitName(p).equals("Scout"))
      KitActions.givePotionEffect(p, PotionEffectType.SPEED, 1);
  }
}


